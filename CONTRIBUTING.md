<!--
  A guide for contributing to this repository

  This material has been released under and is subject to the terms of the
  Common Documentation License, Version 1.0 (the "License").
  The terms of which are hereby incorporated by reference.

  Please obtain a copy of the License in the included file,
  LICENSES/CDL-1.0.txt or online at:

      https://spdx.org/licenses/CDL-1.0.html

  Read the full License text before using this material. Your use of this
  material signifies your agreement to the terms of the License.

  Copyright: 2023 Drew Adams <druonysus@opensuse.org>
  License  : CDL-1.0
  Authors  : Drew Adams
-->
# WORK IN PROGRESS * WORK IN PROGRESS * WORK IN PROGRESS

  _Currently this file is filled with grand desires and flat out lies. Don't trust it! It's a work in progress and it should NOT be relied on. This warning will be removed when it is accurate or at least MORE accurate. Till then, take everything you read here with a grain of salt. Feel free to reach out to the author if you need help._

# WORK IN PROGRESS * WORK IN PROGRESS * WORK IN PROGRESS

# Contributing

# Environment Setup

## Installing D Programming Language tools

`xport` have been built successfully using various versions of `dub` and either `dmd` or `ldc2` on a verity of operating systems such as:
* openSUSE Tumbleweed
* Ubuntu 20.04
* FreeBSD 13.1
* macOS 12 (Monterey)


To set up your local workstation for D development, you'll need to install the following tools:

 * `dub`: D package manager & build system
 * `ldc2` or `dmd`: D compilers

You can follow the installation instructions for your specific operating system:

### openSUSE Tumbleweed
```
sudo zypper install dub ldc make
```
### Ubuntu 20.04

```
sudo apt install dub ldc make
```

### FreeBSD STABLE 13.1
```
sudo pkg install dub ldc gmake
```

### macOS 12 (Monterey)
**NOTE**: I have only tested building and using `xport` on an Intel-based macOS 12 (Monterey) system. I assume changes will be required to build on Apple Silicon.
```
brew update
brew install dub ldc make
```

# Git Guide

## Clone the Repository

**NOTE:** The defalt branch for this repository is `main`

To contribute to `xport`, follow these steps:

1. Fork the repository on GitLab by clicking the "Fork" button on the top-right corner of the `xport` repository page.

2. Clone the forked repository to your local workstation using Git. Replace "[your username]" with your GitLab username.

```
git clone git@gitlab.com:[your username]/xport.git
cd xport
git config user.name [your name]
git config user.email [your email address]
```

3. Set up a remote to track the upstream repository. This allows you to pull any changes made to the main repository into your local fork.
```
git remote add upstream git@gitlab.com:druonysus/xport.git
```
## Branching and Releases

To manage changes, we use a branching and release strategy that enables us to track changes, isolate features, and manage releases. The workflow consists of three types of branches:

* **`main` Branch**: The `main` branch is the primary branch that represents the latest stable release of `xport`. We use this branch for production releases and long-lived development branches.

* **`release/*` Branches**: Release branches are created by the maintainer from the `main` branch and represent a specific version of `xport` using Semantic Versioning. They are used to prepare for a new release by incorporating all the changes and features for the next release. Release branches are named in the following format: `release/[major].[minor]`. For example, a release branch for version `1.2.0` would be named `release/1.2`.

* **`feature/*` Branches**: Feature branches are created from a release branch and represent a specific feature or change to be added to xport. They are used to isolate changes and to allow multiple developers to work on different features at the same time. Feature branches are named in the following format: `feature/[name-of-feature]`. For example, a feature branch for making the code more object-oriented might be named `feature/objectify`.

* **`hotfix/*` Branches**: Hotfix branches are created to fix critical bugs in the current stable release. They are named using Semantic Versioning, in the following format: `hotfix/[major].[minor].[patch]`. For example, a hotfix branch for version 1.2.1 would be named `hotfix/1.2.1`.

### Workflow for Regular Releases

Here's an overview of the workflow for working with branches and releases:

1. **Create a Release Branch**: When it's time to prepare for a new release, the maintainer of the upstream repository will create a new release branch from the latest `main` branch. The version number of the release branch is determined using [Semantic Versioning](https://semver.org/). For example, if the current stable release is version `1.2.1` and the next release will be a minor release, create a new release branch named `release/1.3`.
```
git checkout main
git pull
git checkout -b release/1.3
```

2. **Create Feature Branches**: Create feature branches from the new release branch to work on specific features or changes. For example, if you're working on making the code more object-oriented, create a new feature branch named `feature/objectify` from the `release/1.3` branch.

```
git checkout release/1.3
git pull
git checkout -b feature/objectify
```
3. **Make Changes**: Make changes to the code and commit them to the feature branch. Use descriptive commit messages.

```
git add [modified files]
git commit -m "Add object-oriented support to version module"
```
4. **Merge Feature Branches**: Once the feature is complete and tested, create a Merge Request to merge the feature branch back into the release branch. This integrates the feature into the release and ensures that it's included in the next release.
```
git push origin feature/objectify
```
Then, go to GitLab and create a new Merge Request from the feature branch to the release branch. Assign reviewers and provide a description of the changes.

5. **Prepare for Release**: Once all the features for the release have been merged into the release branch, it's time for the upstream maintainer to prepare for the release. Update the version number in the `dub.sdl` file (and in any other file where it occurs) to reflect the new release version
```
# Update the Version Number
sed -i 's/version "[0-9.]\+"/version "[new version number]"/' dub.sdl
# Add and Commit Changes
git add dub.sdl
git commit -m "Prepare for release [new version number]"
# Push Changes to Your Fork
git push origin release/[new version number]
```
Then, go to GitLab and create a new Merge Request from the release branch to the main branch. Assign reviewers and provide a description of the changes.

6. **Merge to Main Branch**: Once the release is ready, the maintainer will create a Merge Request to merge the release branch into the main branch. This integrates all the changes and features into the latest stable release.

7. **Tag the Release**: Once the release branch has been successfully merged into the `main` branch, The release will be tagged by the maintainer.
```
git checkout main
git tag [new version number]
git push upstream --tags
```



# Licensing Guide

This section provides a guide to help contributors understand what licenses are used in this repository, when to use them, and what kind of comment header to include in their code, if applicable. 

Please refer to the [`LICENSES/`](LICENSES/) directory for the full text of the licenses used in this repository. When contributing, make sure you understand and follow this licensing guide. Here are some tips:

* Only contribute code and files that you have the right to distribute under the chosen license.
* When reusing code or files from other sources make sure that the license is compatable with the chosen license. If you're unsure, please consult with both project's maintainers.
* Make sure to include the appropriate license header in each file you contribute. Use the examples provided in this section as a reference.
* If you are unsure which license to use for a particular file, consult with the project maintainer.
* If you are contributing documentation, make sure to include the appropriate license header in each file. Use the example provided in this section as reference.
* If you are contributing images or other media files, make sure to include the appropriate license information in an adjacent file with the additional extension .`license`.


##  Software Licenses
---
### [Common Development and Distribution License (CDDL), version 1.0](https://opensource.org/licenses/cddl1)
---
This license is used for the application code and tightly coupled related files. You should include the following comment header at the beginning of each file:

```
/**
*
* <one line summary of the code>
*   
* This file and its contents are distributed under the terms of the
* Common Development and Distribution License, Version 1.0 (the "License").
* You may not use this file except in compliance with the License.
*
* You can obtain a copy of the License in the included file,
* LICENSES/CDDL-1.0.txt or online at:
*
*     http://www.opensource.org/licenses/cddl1
*
* See the License for the specific language governing permissions and
* limitations.
* 
* Copyright: [year] [your name]
* License: CDDL-1.0
* Authors: [your name]
*
*/
```
**Example**:
* [`src/main.d`](src/main.d)
---
### [Boost Software License (BSL), version 1.0](https://opensource.org/license/bsl1)
---
This license is used for supporting files not tightly coupled to the application code or not highly differentiated, such as simple Makefiles or helper scripts. You should include the following comment header at the beginning of each file:
```
#
# <one line summary of the code>
#
# This file and its contents are distributed under the terms of the
# Boost Software License, Version 1.0 (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the License in the included file,
# LICENSES/BSL-1.0.txt or online at:
#
#     http://www.opensource.org/licenses/bsl1
#
# See the License for the specific language governing permissions and
# limitations.
#
# Copyright: [year] [your name]
# License  : BSL-1.0
# Authors  : [your name]
#
```
**Example**:
* [`Makefile`](Makefile)

---
### [Unlicense](https://opensource.org/licenses/unlicense)
---
This license is used for examples. You should include the following comment header at the beginning of each file:
```
#
# <one line summary of the code>
#
# This file and its contents are released into the public domain per
# the terms of the Unlicense license (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the License in the included file,
# LICENSES/Unlicense.txt or online at:
#
#     http://www.opensource.org/licenses/unlicense
#
# See the License for the specific language governing permissions and
# limitations.
#
# Copyright: [year] [your name]
# License  : Unlicense
# Authors  : [your name]
#
```
**Example**:
* [`examples/.fake.env`](examples/.fake.env)


## Documentation Licenses
---
### Common Documentation License (CDL), version 1.0
---
This license is used for documentation. You should include the following comment header at the beginning of each file:
```
<!--
  <one line summary of the code>

  This material has been released under and is subject to the terms of the
  Common Documentation License, Version 1.0 (the "License").
  The terms of which are hereby incorporated by reference.

  Please obtain a copy of the License in the included file,
  LICENSES/CDL-1.0.txt or online at:

      https://spdx.org/licenses/CDL-1.0.html

  Read the full License text before using this material. Your use of this
  material signifies your agreement to the terms of the License.

  Copyright: [year] [your name]
  License  : CDL-1.0
  Authors  : [your name]
-->
```
**Example**:
* [`README.md`](README.md)
---
Copyright © 2023 by Drew Adams <<druonysus@opensuse.org>>.

This material has been released under and is subject to the terms of the Common Documentation License, v.1.0, the terms of which are hereby incorporated by reference. Please obtain a copy of the License at https://spdx.org/licenses/CDL-1.0.html and read it before using this material. Your use of this material signifies your agreement to the terms of the License.