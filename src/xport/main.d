/**
* xport's main function
*
* This file and its contents are distributed under the terms of the
* Common Development and Distribution License, Version 1.0 (the "License").
* You may not use this file except in compliance with the License.
*
* You can obtain a copy of the License in the included file,
* LICENSES/CDDL-1.0.txt or online at:
*
*     http://www.opensource.org/licenses/cddl1
*
* See the License for the specific language governing permissions and
* limitations.
*
* Copyright: 2023 Drew Adams <druonysus@opensuse.org>
* License: CDDL-1.0
* Authors: Drew Adams
*
*/
module xport.main;

// import from Phobos
import std.algorithm;
import std.array;
import std.string;
import std.conv;
import std.getopt;
import std.format;
import std.file;
import std.process;
import std.stdio;
import std.uni;
import core.stdc.stdlib : exit;

// import from this project
import xport.ver;
import xport.shellenvvar;

//
// Set defaults for the commandline arguments
/////
string environment = "dev";
string directory = "./";
bool clear = false;
bool ver = false;
string file;
string shell = "sh";

void writeVersion(string progName, string verNumber)
{
    writeln(progName, " ", verNumber, "\n");
    writeln("Copyright   : 2023 Drew Adams <druonysus@opensuse.org>");
    writeln("License     : CDDL 1.0 <https://opensource.org/licenses/cddl1>");
    writeln("Source Code : https://gitlab.com/druonysus/xport");
}

static int main(string[] args)
{
    // establish program name and version info
    string program = args[0].split("/")[$ - 1];
    Version versionInfo = new Version(0, 0, 7);

    //
    // Handle the help info and option parsing
    ///////
    auto helpInformation = getopt(
        args,
        "env|e", "Specify the target environment. Default: 'dev'", &environment,
        "file|f", "Specify the environment file name.", &file,
        "dir|d", "Specify the directory where to search for the environment file.", &directory,
        "shell|s", "Specify the target shell. One of: 'sh', 'csh', 'fish', 'xonsh', 'powershell'", &shell,
        "version|v", "Print version number and exit.", &ver,
        "clear|c", "If 'true' clear environment variables instead of set them.", &clear);

    //
    // Write help when it is asked for and exit
    ///////
    if (helpInformation.helpWanted)
    {
        defaultGetoptPrinter(
                format("%s: Write shell-neutral environment variables and export them in bulk to a variety of shells.", program),
                helpInformation.options);
            exit(1);
    }

    //
    // Write the version information when it is asked for and exit
    ///////
    if (ver) {
	    writeVersion(program, versionInfo.to!string);
            exit(1);
    }

    // figure out the name of the environment file to parse
    file = (file.length == 0 || file is null) ? format("%s.%s.env", directory, environment) : file; 

    //
    // Get the conent of the enfironment file so we can parse next
    ///////
    string fileContent;
    
    try
    {
        fileContent = readText(file);
    }
    catch(FileException err)
    {
	// if getting the content of the file fails, let just print the error message
        stderr.writeln(err.msg);
        exit(100);
    }
   
    
    // lets just make sure the shell string is in all lowercase before we use it
    string shell = shell.toLower();
    auto shellEnvVar = new ShellEnvVar(shell);

    //
    // Parse the content of the environment file, line by line.
    ///////
    foreach (line; fileContent.split("\n"))
    {
        if ( line.startsWith("#") || line.length == 0 )
        {
	    // move on if the line is a comment or empty
            continue;
        }

	string cleanLine = line.split("#")[0];
        string[] kv = cleanLine.split("=");
        string key = kv[0];
        string value = kv[1];

	//
	// Determine if we need to print the set syntax or the clear syntax
	// and then write it.
	///////
	try
	{
            if (!clear)
            {
                writeln(shellEnvVar.getSetSyntax(key, value));
            }
            else
            {
                writeln(shellEnvVar.getClearSyntax(key));
            }
    	}
	catch (Exception err)
	{
	    // if either of those fail, lets just print the error message
	    stderr.writeln(err.msg);
	    exit(100);
	}
    }

    return 0;
}
