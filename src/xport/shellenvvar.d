/**
* ShellEnvVar object for xport
*   
* This file and its contents are distributed under the terms of the
* Common Development and Distribution License, Version 1.0 (the "License").
* You may not use this file except in compliance with the License.
*
* You can obtain a copy of the License in the included file,
* LICENSES/CDDL-1.0.txt or online at:
*
*     http://www.opensource.org/licenses/cddl1
*
* See the License for the specific language governing permissions and
* limitations.
* 
* Copyright: 2023 Drew Adams <druonysus@opensuse.org>
* License: CDDL-1.0
* Authors: Drew Adams
*/    
module xport.shellenvvar;

class ShellEnvVar {
    public string Shell;

    this(string shell) {
        this.Shell = shell;
    }

    string getClearSyntax(string key) {
        switch (this.Shell) {
            case "sh", "bash", "zsh", "ash", "dash":
                return "unset " ~ key;
            case "csh", "tcsh":
                return "unsetenv " ~ key;
            case "pwsh", "posh", "powershell":
                return "Clear-Item Env:/" ~ key;
            case "fish":
                return "set --erase " ~ key;
            case "xonsh":
                return "del $" ~ key;
            default:
                throw new Exception("unsupported shell");
        }
    }

    string getSetSyntax(string key, string value) {
        switch (this.Shell) {
            case "sh", "bash", "zsh", "ash", "dash":
                return "export " ~ key ~ "=" ~ value;
            case "csh", "tcsh":
                return "setenv " ~ key ~ " " ~ value;
            case "pwsh", "posh", "powershell":
                return "$env:" ~ key ~ " = " ~ value;
            case "fish":
                return "set -gx " ~ key ~ " " ~ value;
            case "xonsh":
                return "$" ~ key ~ " = " ~ value;
            default:
                throw new Exception("unsupported shell");
        }
    }
}

