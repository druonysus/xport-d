#
# rpm spec file for xport
#
# This file and its contents are distributed under the terms of the
# Common Development and Distribution License, Version 1.0 (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the License in the included file,
# LICENSES/CDDL-1.0.txt or online at:
#
#     http://www.opensource.org/licenses/cddl1
#
# See the License for the specific language governing permissions and
# limitations.
#
# Copyright: 2023 Drew Adams <druonysus@opensuse.org>
# License  : CDDL-1.0
# Authors  : Drew Adams
#

# LINKS:
#   https://rpm-packaging-guide.github.io/
#   https://en.opensuse.org/openSUSE:Packaging_guidelines
#   https://en.opensuse.org/openSUSE:Specfile_guidelines
#   https://docs.google.com/spreadsheets/u/0/d/14AdaJ6cmU0kvQ4ulq9pWpjdZL5tkR03exRSYJmPGdfs/pub

%define reporoot %(dirname %(readlink -e ..))
%define tempdir %(mktemp -d)
%define instdir %(mktemp -d)
#define _topdir {tempdir}

%define packager "Drew Adams <druonysus@opensuse.org>"

Name: xport
Version: %(grep "^version" %{reporoot}/dub.sdl | awk '{print $1}')
Release: 1%{?dist}
License: CDDL
Summary: Write shell-neutral environment variables and export them in bulk to a variety of shells.
URL: https://gitlab.com/druonysus/xport
Source: %{name}-%{version}.tar.gz
BuildRequires: dmd
BuildRequires: dub
BuildRequires: make
BuildRoot: %{tempdir}

%description
xport enables you to define "environment files" containing simple key-value pairs and generates the appropriate "export" syntax for various shells so you may use those as environment variables. This simplifies switching between different environments, and allows you to bulk set or bulk clear all environment variables defined in a given environment file.

%prep
%setup -q -n %{name}-%{version}
dub build --build=release --single xport
tar -czf %{_sourcedir}/%{name}-%{version}.tar.gz xport

%build
dub build --build=release

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
mkdir -p %{buildroot}/usr/share/man/man1
mkdir -p %{buildroot}/usr/share/doc/%{name}

# Build xport and install it to the buildroot directory
dub build --build=release --single xport
install -m 0755 xport/xport %{buildroot}/usr/bin

# Compress the man page and install it to the buildroot directory
gzip -9c man/xport.1 > %{buildroot}/usr/share/man/man1/%{name}.1.gz

# Copy the xport directory to the buildroot directory
cp -va xport %{buildroot}/usr/share/xport
dos2unix -q -n README.md %{buildroot}/usr/share/doc/%{name}/README
chmod 0644 %{buildroot}/usr/share/doc/%{name}/README

%files
%defattr(-,root,root)
%doc README.md
/usr/bin/xport
/usr/share/man/man1/xport.1.gz
/usr/share/xport
%license %{_datadir}/licenses/%{name}/CDDL-1.0.txt

