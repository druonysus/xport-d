#!/bin/bash
#
# Built xport DEB package
#
#  This file and its contents are distributed under the terms of the
#  Universal Permissive License, Version 1.0 (the "License").
#  You may not use this file except in compliance with the License.
#
#  You may obtain a copy of the License in the included file,
#  LICENSES/UPL-1.0.txt or online at:
#
#     https://opensource.org/license/upl
#
#  See the License for the specific language governing permissions and
#  limitations.
#
#  Copyright: 2023 Drew Adams <druonysus@opensuse.org>
#  License: UPL-1.0
#  Authors: Drew Adams
#
#
umask 0022
status=0

# xport package

INSTDIR=$(mktemp -d)
cp -a pkg/deb/DEBIAN $INSTDIR/DEBIAN

mkdir -p -m 0755 $INSTDIR/usr/share/doc/xport

install -m 0755 bin/xport $INSTDIR/usr/bin
dos2unix -q -n README.md $INSTDIR/usr/share/doc/xport/README
chmod 0644 $INSTDIR/usr/share/doc/xport/README

echo "Installed-Size:" $(du -sx --exclude DEBIAN $INSTDIR | awk '{print $1}') >> $INSTDIR/DEBIAN/control

if ! fakeroot dpkg-deb --build "${INSTDIR}" packages 2> /dev/null
then
	echo "Failed to create xport deb package in directory: ${INSTDIR}"
	let status="${status} + 1"
fi

rm -r "${INSTDIR}"

exit "${status}"
